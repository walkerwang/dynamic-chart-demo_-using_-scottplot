﻿/*----------------------------------------------------------------
* 项目名称 ：WpfApp1.ViewModel
* 类 名 称 ：ScatterPlotListDouble
* 类 描 述 ：
* 命名空间 ：WpfApp1.ViewModel
* 作    者 ：Walker
* 创建时间 ：2023/3/29 13:08:52
* 更新时间 ：2023/3/29 13:08:52
* 版 本 号 ：v1.0.0.0
*******************************************************************
* Copyright @ Walker 2023. All rights reserved.
*******************************************************************/
using ScottPlot.Plottable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.ViewModel
{
    public class ScatterPlotListDouble<T> : ScatterPlotList<T>
    {

        public ScatterPlotListDouble() : base()
        {

        }

        public List<T> GetXs()
        {
            return Xs;
        }

        public List<T> GetYs()
        {
            return Ys;
        }
    }
}
