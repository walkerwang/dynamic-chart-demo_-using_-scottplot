﻿using ScottPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp1.ViewModel;

namespace WpfApp1
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        private ScatterPlotListDouble<double> RealTimeSignalPlot;
        private double[] x_value = new double[] { };
        private double[] y_value = new double[] { };

        public MainWindow()
        {
            InitializeComponent();

            InitSensorData();
        }

        private void InitSensorData()
        {

            RealTimeSignalPlot = new ScatterPlotListDouble<double>()
            {
                MarkerSize = 3,
                Smooth = false,
            };
           

            if (RealTimeSignalPlot.Count != 0)
            {
                x_value = RealTimeSignalPlot.GetXs().ToArray();
                y_value = RealTimeSignalPlot.GetYs().ToArray();

                RealTimeSignalPlot.Clear();
            }
            else
            {
                RealTimeSignalPlot.Add(DateTime.Now.ToOADate(), 0);
            }

            RealTimeSignalPlot.AddRange(x_value, y_value);

            RealTimeContentPlot.Plot.Add(RealTimeSignalPlot);

            RealTimeContentPlot.Plot.XAxis.DateTimeFormat(true);

            RealTimeContentPlot.Plot.AxisAuto();
            RealTimeContentPlot.Refresh();
        }
        private readonly Random random = new Random();
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            double x = DateTime.Now.ToOADate();
            double y = DataGen.RandomNormalValue(random, 0.5, 1);
            RealTimeSignalPlot.Add(x,y);

            if (RealTimeSignalPlot.Count > 50)
            {
                RealTimeSignalPlot.GetXs().RemoveAt(0);
                RealTimeSignalPlot.GetYs().RemoveAt(0);
            }

            RealTimeContentPlot.Plot.AxisAuto();
            RealTimeContentPlot.Refresh();
        }
    }
}
